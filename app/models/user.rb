class User < ActiveRecord::Base
  belongs_to :client
  has_many :bookings

  validates :email, uniqueness: true
  validates :email, :invoice_email, format: { with: Devise.email_regexp, allow_blank: true }
end