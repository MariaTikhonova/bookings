class CreateUsers < ActiveRecord::Migration
    def change
      create_table :users do |t|
        t.string :name
        t.string :email
        t.integer :client_id
  
        t.timestamps
      end
      add_index :users, :email, unique: true, algorithm: :concurrently
    end
  end
  