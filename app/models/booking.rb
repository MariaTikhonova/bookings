# this class holds Table settings
class Booking < ActiveRecord::Base
  
  belongs_to :client
  belongs_to :user
  belongs_to :status

  scope :inprocess, -> { where(status_id: Status.inprocess.id) }
  scope :not_inprocess, -> { where("bookings.status_id <> ?", Status.inprocess.id) }
  scope :active, -> { Booking.where("bookings.start_time >= (NOW() - INTERVAL '30 minutes')") }
  validates :start_time, :user_id, presence: true, if: :inprocess?
  before_save :timing_for_schedule, if: :active?
    
  def prevent_overlapping
    bookings.each do |booking|
      if self.start_time <= booking.start_time && self.end_time >= booking.end_time
        booking.destroy
        errors.add(:start_time, "Booking #{self.id} is not valid:" + "time is taken")
      end
    end
  end

  def timing_for_schedule
    schedule = ClientCustomizing.first
    if self.start_time < schedule.start_time && self.end_time > schedule.end_time
      booking.destroy
      errors.add(:start_time, "Booking #{self.id} is not valid:" + "We are closed at that time!")
    end
  end

  def timing_for_midnight
    schedule = ClientCustomizing.first
    if schedule.end_time >= (schedule.start_time + 1.day).beginning_of_day
      if end_time.present? && start_time.to_date != end_time.to_date && end_time != (start_time + 1.day).beginning_of_day
        booking = self.dup
        self.end_time = (start_time + 1.day).beginning_of_day
        booking.start_time = self.end_time
        booking.save!
      end
    end
  end
end