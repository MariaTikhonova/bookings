class Createbookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.datetime "start_time"
      t.datetime "end_time"
      t.references :user, foreign_key: true
      t.references :client, foreign_key: true
      t.references :status, foreign_key: true
      
      t.timestamps
    end
    add_index :bookings, :user_id
  end
end
