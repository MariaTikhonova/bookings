class Client < ActiveRecord::Base

  # this class handles Restaurant settings 
  
  has_one :schedule
  has_many :users

  validates :name, presence: true
end