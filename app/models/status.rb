class Status < ActiveRecorf::Base
  
  def self.inprocess
    @inprocess ||= Status.where(code: "INPROCESS").first
  end
end