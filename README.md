# README

Features and additions could be made in additional time:

* To substitute conditions and repetitions for Customizing ( schedule) at Booking model, we can trigger something like customizing scope at Booking. 

* we can substitute or remove before_save callback depending on context

* Booking queries and generation should be processed apart at services/bookings 

* And then triggered by jobs at controller

* Unit specs should be added with factories
