#this class holds schedule settings
class ClientCustomizing < ActiveRecord::Base
  validates :start_time, :end_time, presence: true
end